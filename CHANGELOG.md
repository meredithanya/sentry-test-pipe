# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.15

- patch: no{

## 0.0.14

- patch: use actual environment

## 0.0.13

- patch: trying this out

## 0.0.12

- patch: test release

## 0.0.11

- patch: ssd

## 0.0.10

- patch: sdf

## 0.0.9

- patch: ugh

## 0.0.8

- patch: f

## 0.0.7

- patch: not jon snow

## 0.0.6

- patch: who knows

## 0.0.5

- patch: something

## 0.0.4

- patch: use sentry-cli image

## 0.0.3

- patch: add sentry org

## 0.0.2

- patch: Test this out again

## 0.0.1

- patch: Test this out

